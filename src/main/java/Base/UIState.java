package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UIState extends BaseState {

    private int defaultTimeout = 5;

    /**
     * Navigates to specified url
     *
     * @param url Url page is navigating to
     */
    protected void navigateTo(String url) {
        driver.get(url);
        if (this.driver != null) {
            new WebDriverWait(this.driver, 10).until(
                    webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        }
    }

    /**
     * Gets page title
     *
     * @return String
     */
    protected String getTitle() {
        return driver.getTitle();
    }

    /**
     * Clicks on specified element
     *
     * @param selector Element selector
     */
    protected void click(String selector) {
        click(selector, defaultTimeout);
    }

    /**
     * Waits presence of the specified element than clicks it
     *
     * @param selector Element selector
     * @param timeout  Time of waiting
     */
    protected void click(String selector, int timeout) {
        WebElement el = (new WebDriverWait(driver, timeout))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(selector)));
        el.click();
    }

    /**
     * Enters text in specified element
     *
     * @param selector Element selector
     * @param text     The text that will be entered in specified element
     */
    protected void sendKeys(String selector, String text) {
        sendKeys(selector, text, defaultTimeout);
    }

    /**
     * Waits presence of the specified element then enters text in it
     *
     * @param selector Element selector
     * @param text     The text that will be typed in specified element
     * @param timeout  Time of waiting
     */
    protected void sendKeys(String selector, String text, int timeout) {
        WebElement el = (new WebDriverWait(driver, timeout))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(selector)));
        el.sendKeys(text);
    }

    /**
     * Checks if the element is presented
     *
     * @param selector Element selector
     * @return boolean
     */
    protected boolean isElementPresent(String selector) {
        try {
            driver.findElement(By.xpath(selector));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Gets text from the element
     *
     * @param selector Element selector
     * @return String
     */
    protected String getText(String selector) {
        return driver.findElement(By.xpath(selector)).getText();
    }

    /**
     * Scrolls to element
     *
     * @param xpath Xpath of the element we need to get text from
     */
    protected void scrollToElement(String xpath) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(xpath)));
    }

}
