package Base;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseState {

    protected WebDriver driver = null;

    @BeforeMethod
    protected void setUp() {
        System.setProperty("webdriver.chrome.driver", "target/drivers/chromedriver.exe");
        String browser = (System.getProperty("browser") == null) ? "chrome" : System.getProperty("browser");
        switch (browser) {
            case "chrome":
                this.driver = new ChromeDriver();
                this.driver.manage().window().maximize();
                break;
            case "mobilechrome":
                this.driver = new ChromeDriver();
                this.driver.manage().window().setSize(new Dimension(400, this.driver.manage().window().getSize().height));
        }
    }

    @AfterMethod
    protected void tearDown() {
        this.driver.quit();
    }

}
