package Base;

import dataItems.utils.HttpResponse;
import dataItems.utils.Log;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

import java.util.List;

public class APIState {

    /**
     * Making GET request with url and headers
     *
     * @param url     Url for making request
     * @param headers List of headers for request
     * @return HttpResponse
     * @throws Exception
     */
    private static HttpResponse mainGet(String url, List<Header> headers) throws Exception {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet get = new HttpGet(url);
        if (headers != null)
            headers.forEach(get::addHeader);
        Log.info(String.format("Making get request to: %s", url));
        CloseableHttpResponse response = client.execute(get);
        return new HttpResponse(response);
    }

    public static HttpResponse get(String url) throws Exception {
        return mainGet(url, null);
    }

    public static HttpResponse get(String url, List<Header> headers) throws Exception {
        return mainGet(url, headers);
    }

    /**
     * Making GET request with url and headers
     *
     * @param url     Url for making request
     * @param headers List of headers for request
     * @param body    Body of POST request of String type
     * @return HttpResponse
     * @throws Exception
     */
    public static HttpResponse mainPost(String url, List<Header> headers, String body) throws Exception {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(url);
        StringEntity entity = new StringEntity(body, "UTF8");
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        post.setEntity(entity);
        CloseableHttpResponse response = client.execute(post);
        if (headers != null)
            headers.forEach(post::addHeader);
        Log.info(String.format("Making post request to: %s", url));
        Log.info(String.format("Request body is:%s", body));
        return new HttpResponse(response);
    }

    public static HttpResponse post(String url, String body) throws Exception {
        return mainPost(url, null, body);
    }

    public static HttpResponse post(String url, List<Header> headers, String body) throws Exception {
        return mainPost(url, headers, body);
    }

}
