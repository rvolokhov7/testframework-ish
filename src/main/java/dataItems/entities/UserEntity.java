package dataItems.entities;

import com.google.gson.GsonBuilder;

public class UserEntity {

    User data;

    public UserEntity(Integer id, String name, String type) {
        data = new User(id, name, type);
    }

    class User {
        Integer id;
        String name;
        String type;

        public User(Integer id, String name, String type) {
            this.id = id;
            this.name = name;
            this.type = type;
        }
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

}
