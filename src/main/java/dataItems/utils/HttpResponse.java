package dataItems.utils;

import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.ByteArrayOutputStream;

public class HttpResponse {

    private String statusLine;
    private int statusCode;
    private String body;

    public HttpResponse(CloseableHttpResponse response) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            response.getEntity().writeTo(stream);
        } catch (Exception e) {
        }
        this.body = new String(stream.toByteArray());
        this.statusLine = response.getStatusLine().toString();
        this.statusCode = response.getStatusLine().getStatusCode();
    }

    public String getBody() {
        return body;
    }

    public String getStatusLine() {
        return statusLine;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
