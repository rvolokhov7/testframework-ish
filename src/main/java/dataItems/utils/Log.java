package dataItems.utils;

import org.apache.log4j.Logger;

public class Log {

    final static Logger logger = Logger.getLogger(Log.class);

    public static void info(String msg) {
        logger.info(msg);
    }

    public static void debug(String msg) {
        logger.debug(msg);
    }

    public static void warn(String msg) {
        logger.debug(msg);
    }

    public static void error(String msg) {
        logger.debug(msg);
    }

}
