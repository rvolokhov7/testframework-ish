package dataItems.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    public static Properties prop;

    public static String getProperty(String propertyKey) {
        return prop.getProperty(propertyKey);
    }

    static {
        InputStream input;
        try {
            input = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/config.properties");
            prop = new Properties();
            prop.load(input);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

}
