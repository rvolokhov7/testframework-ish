package dataItems.utils;

import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider(name = "invalidCredentials")
    public static Object[][] invalidCredentials() {
        return new Object[][]{{"login1", "pass1"}, {"login2", "pass2"}, {"", ""}};
    }

}
