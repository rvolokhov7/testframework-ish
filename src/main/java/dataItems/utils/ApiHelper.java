package dataItems.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ApiHelper {

    public static String buildUrlParameters(String rootPath, Map<String, Object> paramsMap) {
        Set<Map.Entry<String, Object>> set = paramsMap.entrySet();
        Map<String, Object> newParamsMap = new HashMap<>();
        for (Map.Entry<String, Object> me : set) {
            if (me.getValue() != null)
                newParamsMap.put(me.getKey(), me.getValue());
        }
        String urlParameters = newParamsMap.toString()
                .replaceAll(", ", "&")
                .replaceAll("[{}]", "");
        if (!newParamsMap.isEmpty())
            urlParameters = "?" + urlParameters;
        return rootPath + urlParameters;
    }

}
