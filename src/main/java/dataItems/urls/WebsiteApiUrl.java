package dataItems.urls;

import java.util.HashMap;
import java.util.Map;

import static dataItems.utils.ApiHelper.buildUrlParameters;
import static dataItems.utils.PropertyReader.getProperty;

public class WebsiteApiUrl {

    public String ROOT = getProperty("apiPageUrl");

    /*******************************
     ********* Parameters **********
     *******************************/

    private static final String USER_ID_PARAMETER = "user_id";

    /******************************
     ********* Variables **********
     ******************************/

    private String clientIdParameter = null;

    public WebsiteApiUrl() {
    }

    public WebsiteApiUrl(String clientId) {
        setClientId(clientId);
    }

    public WebsiteApiUrl setClientId(String clientId) {
        this.clientIdParameter = clientId;
        return this;
    }

    public String getClientId() {
        return this.clientIdParameter;
    }

    @Override
    public String toString() {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put(USER_ID_PARAMETER, clientIdParameter);
        return buildUrlParameters(ROOT, paramsMap);
    }


}
