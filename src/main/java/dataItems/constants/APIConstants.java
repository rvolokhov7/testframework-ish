package dataItems.constants;

public class APIConstants {

    public static class HTTPResponseCode {
        public static final String CODE_200 = "HTTP/1.1 200 OK";
        public static final String CODE_422 = "HTTP/1.1 422 Unprocessable Entity";
    }

    public static class Titles {
    }

}
