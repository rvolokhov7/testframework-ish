package Tests.API;

import Base.APIState;
import dataItems.entities.UserEntity;
import dataItems.urls.WebsiteApiUrl;
import dataItems.utils.HttpResponse;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static dataItems.constants.APIConstants.HTTPResponseCode.CODE_200;
import static dataItems.constants.APIConstants.HTTPResponseCode.CODE_422;

public class WebsiteAPITest {

    @Test
    public void testGetPing() throws Exception {
        String url = new WebsiteApiUrl().toString();
        HttpResponse response = APIState.get(url);
        Assert.assertEquals(response.getStatusLine(), CODE_200,
                "Wrong response status");
        Assert.assertEquals(new JSONObject(response.getBody()).getBoolean("pong"), true,
                "'pong' field has wrong value");
    }

    @Test
    public void testGetValidUserId() throws Exception {
        int userId = 123;
        String url = new WebsiteApiUrl(String.valueOf(userId)).toString();
        HttpResponse response = APIState.get(url);
        Assert.assertEquals(response.getStatusLine(), CODE_200,
                "Wrong response status");
        Assert.assertEquals(new JSONObject(response.getBody()).getInt("id"), userId,
                "field 'id' has wrong value");
        Assert.assertFalse(new JSONObject(response.getBody()).isNull("name"),
                "field 'name' has wrong value");
        Assert.assertFalse(new JSONObject(response.getBody()).isNull("type"),
                "field 'type' has wrong value");
    }

    @Test
    public void testGetWrongUserId() throws Exception {
        String userId = "111";
        String url = new WebsiteApiUrl(userId).toString();
        HttpResponse response = APIState.get(url);
        Assert.assertEquals(response.getStatusLine(), CODE_422,
                "Wrong response status");
        Assert.assertEquals(new JSONObject(response.getBody()).getJSONObject("error").getString("message"),
                "wrong id",
                "wrong error message");
        Assert.assertEquals(new JSONObject(response.getBody()).getJSONObject("error").getString("title"),
                "something goes wrong",
                "wrong error title");
    }

    @Test
    public void testPostCorrectJsonScheme() throws Exception {
        String url = new WebsiteApiUrl().toString();
        UserEntity userEntity = new UserEntity(7, "name", "type");
        HttpResponse response = APIState.post(url, userEntity.toString());
        Assert.assertEquals(response.getStatusLine(), CODE_200,
                "Wrong response status");
        Assert.assertEquals(new JSONObject(response.getBody()).getBoolean("pong"), true,
                "'pong' field has wrong value");
    }

    @Test
    public void testPostWrongJsonScheme() throws Exception {
        String url = new WebsiteApiUrl().toString();
        UserEntity userEntity = new UserEntity(null, "name", "type");
        HttpResponse response = APIState.post(url, userEntity.toString());
        Assert.assertEquals(response.getStatusLine(), CODE_422,
                "Wrong response status");
        Assert.assertEquals(new JSONObject(response.getBody()).getJSONObject("error").getString("message"),
                "wrong id",
                "wrong error message");
        Assert.assertEquals(new JSONObject(response.getBody()).getJSONObject("error").getString("title"),
                "something goes wrong",
                "wrong error title");
    }

}
