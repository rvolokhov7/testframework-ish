package Tests.UI;

import Base.UIState;
import Pages.MainPage;
import dataItems.utils.Log;
import org.testng.Assert;
import org.testng.annotations.Test;


public class MainPageTest extends UIState {

    @Test
    public void testTitle() {
        Log.info("Go to main page");
        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(getTitle(), mainPage.TITLE, "Wrong title");
    }

    @Test
    public void testHeader() {
        Log.info("Go to main page");
        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(getText(mainPage.HEADER_ELEMENT), mainPage.HEADER, "Wrong header text");
    }

}