package Tests.UI;

import Base.UIState;
import Pages.LoginPage;
import Pages.MainPage;
import dataItems.utils.DataProviderClass;
import dataItems.utils.Log;
import org.testng.Assert;
import org.testng.annotations.Test;

import static dataItems.utils.PropertyReader.getProperty;


public class LoginPageTest extends UIState {

    @Test
    public void testTitle() {
        Log.info("Go to login page");
        LoginPage loginPage = new LoginPage(driver);
        Assert.assertEquals(getTitle(), loginPage.TITLE, "Wrong title");
    }

    @Test
    public void testControls() {
        Log.info("Go to login page");
        LoginPage loginPage = new LoginPage(driver);
        Assert.assertTrue(isElementPresent(loginPage.LOGIN_INPUT), "There is no login input on the login page");
        Assert.assertTrue(isElementPresent(loginPage.PASSWORD_INPUT), "There is no password input on the login page");
    }

    @Test
    public void testLoginSuccess() {
        String login = getProperty("login");
        String password = getProperty("password");
        Log.info("Go to login page");
        LoginPage loginPage = new LoginPage(driver);
        Log.info("Type in valid user login and password");
        sendKeys(loginPage.LOGIN_INPUT, login);
        sendKeys(loginPage.PASSWORD_INPUT, password);
        click(loginPage.OK_BUTTON);
        Log.info("Go to main page");
        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(getTitle(), mainPage.TITLE, "Wrong title");
    }

    @Test(dataProvider = "invalidCredentials", dataProviderClass = DataProviderClass.class)
    public void testLoginFail(String login, String password) {
        Log.info("Go to login page");
        LoginPage loginPage = new LoginPage(driver);
        Log.info("Type in invalid user login and password");
        sendKeys(loginPage.LOGIN_INPUT, login);
        sendKeys(loginPage.PASSWORD_INPUT, password);
        click(loginPage.OK_BUTTON);
        Assert.assertEquals(getText(loginPage.MESSAGE), loginPage.ERROR_MESSAGE, "Wrong error message");
    }

}