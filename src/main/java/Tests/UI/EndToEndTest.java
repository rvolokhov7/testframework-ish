package Tests.UI;

import Base.UIState;
import Pages.LoginPage;
import Pages.MainPage;
import dataItems.utils.Log;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static dataItems.utils.PropertyReader.getProperty;


public class EndToEndTest extends UIState {

    @Test
    public void testMain() {
        SoftAssert softAssert = new SoftAssert();
        Log.info("Go to login page");
        LoginPage loginPage = new LoginPage(driver);
        softAssert.assertEquals(getTitle(), loginPage.TITLE, "Wrong title");
        softAssert.assertTrue(isElementPresent(loginPage.LOGIN_INPUT), "There is no login input on the login page");
        softAssert.assertTrue(isElementPresent(loginPage.PASSWORD_INPUT), "There is no password input on the login page");
        softAssert.assertTrue(isElementPresent(loginPage.OK_BUTTON), "There is no 'OK' button on the login page");
        String login = getProperty("login");
        String password = getProperty("password");
        Log.info("Type in user credentials and click ok");
        sendKeys(loginPage.LOGIN_INPUT, login);
        sendKeys(loginPage.PASSWORD_INPUT, password);
        click(loginPage.OK_BUTTON);
        Log.info("Go to main page");
        MainPage mainPage = new MainPage(driver);
        softAssert.assertEquals(getTitle(), mainPage.TITLE, "Wrong title");
        softAssert.assertEquals(getText(mainPage.HEADER_ELEMENT), mainPage.HEADER, "Wrong header text");
        softAssert.assertAll();
    }

}