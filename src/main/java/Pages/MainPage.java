package Pages;

import Base.UIState;
import org.openqa.selenium.WebDriver;

import static dataItems.utils.PropertyReader.getProperty;

public class MainPage extends UIState {

    public MainPage(WebDriver driver) {
        this.driver = driver;
        navigateTo(getProperty("mainPageUrl"));
    }

    /*
     * XPATH
     */
    public final String HEADER_ELEMENT = "//h1";

    /*
     * CONSTANTS
     */
    public final String TITLE = "Main Page";
    public final String HEADER = "YOU ARE ALWAYS WELCOME!";

}
