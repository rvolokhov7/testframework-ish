package Pages;

import Base.UIState;
import org.openqa.selenium.WebDriver;

import static dataItems.utils.PropertyReader.getProperty;

public class LoginPage extends UIState {

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        navigateTo(getProperty("loginPageUrl"));
    }

    /*
     * XPATH
     */
    public final String LOGIN_INPUT = "//*[@id='loginInput']";
    public final String PASSWORD_INPUT = "//*[@id='passwordInput']";
    public final String OK_BUTTON = "//input[@name='btn1']";
    public final String MESSAGE = "//*[@id='message']";

    /*
     * CONSTANTS
     */
    public final String TITLE = "WEB SITE";
    public final String ERROR_MESSAGE = "Wrong login or password!";

}
